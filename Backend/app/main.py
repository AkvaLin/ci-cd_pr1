from fastapi import FastAPI
import redis
from random import randint


def generate_sorted_random_array(n_elements):
    return sorted([randint(1, 100) for _ in range(n_elements)])


r = redis.Redis(host='redis', port=6379, decode_responses=True)
arr = []
if r.llen("array") == 0:
    arr = generate_sorted_random_array(100)
    r.rpush("array", *arr)


def binary_search(needle):
    low = 0
    high = r.llen("array")
    idx = (low + high) // 2
    
    while low < high:
        if int(r.lindex("array", idx)) < needle:
            low = idx + 1
        else:
            high = idx
        idx = (low + high) // 2
            
    return idx if int(r.lindex("array", idx)) == needle else -1


app = FastAPI()

@app.get("/find/{number}")
def read_root(number):
    idx = binary_search(int(number))
    return {"index":idx}

