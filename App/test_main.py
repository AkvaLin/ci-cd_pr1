import main

def _is_sorted(array):
    for i in range(len(array)-1):
        if array[i] > array[i+1]:
            return False
    return True

def test_create_array():
    for i in range(500):
        arr = main.generate_sorted_random_array(i)
        assert _is_sorted(arr)
        assert len(arr) == i
        
def test_binary_search():
    for i in range(500):
        arr = main.generate_sorted_random_array(100)
        for elem in arr:
            assert main.binary_search(elem, arr) == arr.index(elem)