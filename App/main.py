from random import randint
import sys


def generate_sorted_random_array(n_elements):
    return sorted([randint(1, 100) for _ in range(n_elements)])

def binary_search(needle, haystack):
    low = 0
    high = len(haystack)
    idx = (low + high) // 2
    
    while low < high:
        if haystack[idx] < needle:
            low = idx + 1
        else:
            high = idx
        idx = (low + high) // 2
            
    return idx if haystack[idx] == needle else None

def main():
    arr = generate_sorted_random_array(100)
    print(binary_search(int(sys.argv[1]), arr))

if __name__ == "__main__":
    main()
